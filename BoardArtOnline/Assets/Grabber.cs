using System.Collections;
using System.Collections.Generic;
using UnityEditor.Callbacks;
using UnityEngine;
using UnityEngine.InputSystem;

public class Grabber : MonoBehaviour
{
    public InputActionReference GrabAction = null;

    public float MaxForce = 20f;
    public float MaxTorque = 20f;
    public bool LockRotation = false;

    private Grabbable grabbedThing = null;

    // Start is called before the first frame update
    void Start()
    {
        GrabAction.action.started += DoGrab;
        GrabAction.action.canceled += StopGrab;
        DoGrab(new InputAction.CallbackContext());
    }

    private void DoGrab(InputAction.CallbackContext obj)
    {
        StopGrab(new InputAction.CallbackContext());

        Collider[] colliders;
        if ((colliders = Physics.OverlapSphere(transform.position, 0.1f)).Length > 0)
        {
            foreach (var collider in colliders)
            {
                if (collider.gameObject == gameObject) continue;

                Rigidbody rb = collider.gameObject.GetComponent<Rigidbody>();
                if (rb && !rb.isKinematic)
                {
                    if (!rb.gameObject.TryGetComponent<Grabbable>(out grabbedThing))
                    {
                        grabbedThing = rb.gameObject.AddComponent<Grabbable>();
                    }
                    grabbedThing.AddGrabber(this);
                    break;
                }
            }
        }
    }

    private void StopGrab(InputAction.CallbackContext obj)
    {
        if (grabbedThing)
        {
            grabbedThing.RemoveGrabber(this);
        }
        grabbedThing = null;
    }
}
