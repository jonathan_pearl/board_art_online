using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    public float MoveSpeed = 1.0f;
    public Transform LookTransform;

    public InputActionReference MoveAction = null;
    public InputActionReference TurnAction = null;

    // Update is called once per frame
    void Update()
    {
        Vector3 right = Vector3.Cross(Vector3.up, LookTransform.forward);
        Vector3 forward = Vector3.Cross(right, Vector3.up);

        Vector2 input = MoveAction.action.ReadValue<Vector2>();
        transform.position += (right * input.x + forward * input.y) * MoveSpeed * Time.deltaTime;
    }
}
