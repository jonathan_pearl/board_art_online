using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabbable : MonoBehaviour
{
    public int SolverIterations = 15;
    public bool CatchUp = false;

    class GrabState
    {
        public Vector3 grabbedOffsetPos;
        public Quaternion grabbedOffsetRot;
        public Vector3? lastFixedUpdate;
    }
    private Dictionary<Grabber, GrabState> grabbedBy = new Dictionary<Grabber, GrabState>();

    private Rigidbody rigidBody = null;
    private Rigidbody CurrentRigidBody { get { if (!rigidBody) rigidBody = GetComponent<Rigidbody>(); return rigidBody; } }

    private float previousMaxAngularVelocity;
    private Vector3? lastFixedUpdate = null;

    public void AddGrabber(Grabber grabber)
    {
        if (grabbedBy.Count == 0)
        {
            previousMaxAngularVelocity = CurrentRigidBody.maxAngularVelocity;

            CurrentRigidBody.maxAngularVelocity = 20;
        }

        GrabState state = new GrabState();
        state.grabbedOffsetPos = transform.InverseTransformPoint(grabber.transform.position);
        state.grabbedOffsetRot = Quaternion.Inverse(grabber.transform.rotation) * transform.rotation;
        grabbedBy.Add(grabber, state);
    }
    public void RemoveGrabber(Grabber grabber)
    {
        grabbedBy.Remove(grabber);

        if (grabbedBy.Count == 0)
        {
            CurrentRigidBody.maxAngularVelocity = previousMaxAngularVelocity;
        }
    }

    static Quaternion FromAngVel(Vector3 angVel)
    {
        float mag = angVel.magnitude;
        return Quaternion.AngleAxis(mag, angVel / mag);
    }

    private void FixedUpdate()
    {
        if (lastFixedUpdate.HasValue) transform.position = lastFixedUpdate.Value;
        lastFixedUpdate = null;

        if (grabbedBy.Count == 0) return;

        // APPARENTLY ANGULAR VELOCITY DOESN'T UPDATE AFTER CALLING ADDTORQUE! NEAT!
        Vector3 velocityAdj = Vector3.zero;
        Vector3 angVelAdj = Vector3.zero;

        // TODO: Use max force/torque per grabber
        for (int i = 0; i < SolverIterations; ++i)
        {
            Vector3 lockRotationTotal = Vector3.zero;
            int lockRotationTotalWeight = 0;

            foreach (var entry in grabbedBy)
            {
                Grabber grabber = entry.Key;
                GrabState state = entry.Value;

                if (grabber.LockRotation)
                {
                    // Rotation
                    var desiredRotation = grabber.transform.rotation * state.grabbedOffsetRot;
                    Quaternion current = transform.rotation;
                    if (Quaternion.Dot(current, desiredRotation) < 0)
                    {
                        current = Quaternion.Inverse(current);
                        current.w *= -1;
                    }
                    var currentRotOffset = desiredRotation * Quaternion.Inverse(current);
                    currentRotOffset.ToAngleAxis(out var ang, out var axis);
                    var desiredAngVel = axis * ang * Mathf.Deg2Rad / Time.fixedDeltaTime;
                    var requiredTorque = desiredAngVel - (CurrentRigidBody.angularVelocity + angVelAdj);
                    lockRotationTotal += requiredTorque;
                    ++lockRotationTotalWeight;
                }
            }
            // Average locked rotation
            if (lockRotationTotalWeight > 0)
            {
                angVelAdj += lockRotationTotal / lockRotationTotalWeight;
            }

            Vector3 velocityTotal = Vector3.zero;
            //Vector3 velocityWorldOrigin = Vector3.zero;
            //float velocityWorldOriginTotalWeight = 0;

            Vector3 preAngVelAdj = angVelAdj;
            foreach (var entry in grabbedBy)
            {
                Grabber grabber = entry.Key;
                GrabState state = entry.Value;

                // Position
                var approachedRotation = FromAngVel((CurrentRigidBody.angularVelocity + preAngVelAdj) * Mathf.Rad2Deg * Time.fixedDeltaTime) * transform.rotation;
                var grabbedOffsetPosWorldspace = transform.position + approachedRotation * Vector3.Scale(transform.lossyScale, state.grabbedOffsetPos);
                Vector3 desiredVel = (grabber.transform.position - grabbedOffsetPosWorldspace) / Time.fixedDeltaTime;
                Vector3 requiredForce = desiredVel - (CurrentRigidBody.velocity + velocityAdj);
                velocityTotal += requiredForce;
                //Debug.DrawRay(grabbedOffsetPosWorldspace, requiredForce, Color.red, Time.fixedDeltaTime);
                //velocityWorldOrigin += grabber.transform.position * requiredForce.magnitude;
                //velocityWorldOriginTotalWeight += requiredForce.magnitude;

                // Floats around
                if (!grabber.LockRotation)
                {
                    Vector3 diff = CurrentRigidBody.worldCenterOfMass - grabbedOffsetPosWorldspace;
                    Vector3 axis = Vector3.Cross(requiredForce, diff).normalized;
                    Vector3 tangent = Vector3.Cross(diff, axis).normalized;
                    angVelAdj += axis * Vector3.Dot(requiredForce, tangent) * 1f;
                }
            }

            // Average point velocities
            velocityTotal /= grabbedBy.Count;
            velocityAdj += velocityTotal;
            //velocityWorldOrigin /= velocityWorldOriginTotalWeight;
        }
        CurrentRigidBody.AddForce(velocityAdj, ForceMode.VelocityChange);
        CurrentRigidBody.AddTorque(angVelAdj, ForceMode.VelocityChange);
    }

    private void Update()
    {
        // Update last fu time for responsive movement
        if (CatchUp && !lastFixedUpdate.HasValue)
        {
            lastFixedUpdate = transform.position;
            foreach (var entry in grabbedBy)
            {
                entry.Value.lastFixedUpdate = entry.Key.transform.position;
            }
        }
    }
    
    private void LateUpdate()
    {
        if (!CatchUp || !lastFixedUpdate.HasValue) return;
    
        Vector3 offset = Vector3.zero;
        int count = 0;
    
        foreach (var entry in grabbedBy)
        {
            if (entry.Value.lastFixedUpdate.HasValue)
            {
                offset += entry.Key.transform.position - entry.Value.lastFixedUpdate.Value;
                count++;
            }
        }
    
        if (count > 0)
        {
            transform.position = lastFixedUpdate.Value + offset / count;
        }
    }
}
